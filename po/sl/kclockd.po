# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kclock package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-08-07 00:43+0000\n"
"PO-Revision-Date: 2022-09-19 12:25+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 22.04.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Matjaž Jeran"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "matjaz.jeran@amis.net"

#: alarm.cpp:73 alarm.h:51
#, kde-format
msgid "Alarm"
msgstr "Alarm"

#: alarm.cpp:77 timer.cpp:67
#, kde-format
msgid "View"
msgstr "Pogled"

#: alarm.cpp:80
#, kde-format
msgid "Dismiss"
msgstr "Opusti"

#: alarm.cpp:83
#, kde-format
msgid "Snooze"
msgstr "Dremež"

#: alarmmodel.cpp:222
#, kde-kuit-format
msgctxt "@info"
msgid "Alarm: <shortcut>%1 %2</shortcut>"
msgstr "Alarm: <shortcut>%1 %2</shortcut>"

#: main.cpp:31
#, kde-format
msgid "Don't use PowerDevil for alarms if it is available"
msgstr "Ne uporabljajte PowerDevil za alarme, če je na voljo"

#: main.cpp:46
#, kde-format
msgid "© 2020-2022 KDE Community"
msgstr "© 2020-2022 Skupnost KDE"

#: main.cpp:47
#, kde-format
msgid "Devin Lin"
msgstr "Devin Lin"

#: main.cpp:48
#, kde-format
msgid "Han Young"
msgstr "Han Young"

#: timer.cpp:62
#, kde-format
msgid "Timer complete"
msgstr "Merilnik časa končal"

#: timer.cpp:63
#, kde-format
msgid "Your timer %1 has finished!"
msgstr "Vaš merilnik časa %1 je končal!"

#: xdgportal.cpp:34
#, kde-format
msgid ""
"Allow the clock process to be in the background and launched on startup."
msgstr "Dovoli, da je postopek ure v ozadju in se zažene na zagonu."
